<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text"/>
 
<xsl:template match="/"><xsl:apply-templates/></xsl:template>

<xsl:template match="note">\<xsl:value-of select="@style"/><xsl:text> </xsl:text><xsl:value-of select="caller"/><xsl:text> </xsl:text><xsl:value-of select="@number"/><xsl:text> </xsl:text><xsl:apply-templates/> <xsl:text> \</xsl:text><xsl:value-of select="@style"/><xsl:text>* </xsl:text></xsl:template>
<xsl:template match="*">\<xsl:value-of select="@style"/><xsl:text> </xsl:text><xsl:value-of select="@number"/><xsl:text> </xsl:text><xsl:apply-templates/></xsl:template>
</xsl:stylesheet>